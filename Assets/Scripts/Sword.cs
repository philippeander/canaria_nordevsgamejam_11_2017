﻿using UnityEngine;
using System.Collections;

public class Sword : MonoBehaviour {

	public SphereCollider sc;
	// Use this for initialization
	void Awake () {
		sc = GetComponent<SphereCollider> ();
	}
	
	// Update is called once per frame
	void Update () {
		sc.center = transform.position;
	}
}
