﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class EnemieAi : MonoBehaviour {

	[SerializeField]private GameObject[] spwRewards = new GameObject[3];
	[SerializeField]private float ProjectilSpeed = 2.0f;
	[SerializeField]private GameObject[] AnyGun = null;
	[SerializeField]private Collider[] AnyCollider = null;

	Transform player;   
	EnSensor sensor;
	EnSphereCol sphereCol;
	NavMeshAgent nav;
	Animator anim;
	bool isWalk, isDead, attack;
	EnemieHealth enemieHealth;

	public bool IsWalk{get{return isWalk;} set{isWalk = value;}}
	public bool IsDead{get{return isDead;} set{isDead = value;}}
	public bool IsAttack{get{return attack;} set{attack = value;}}

	void Awake ()
	{
		enemieHealth = GetComponent<EnemieHealth> ();
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		nav = GetComponent <NavMeshAgent> ();
		sensor = GetComponentInChildren<EnSensor> ();
		sphereCol = GetComponentInChildren<EnSphereCol> ();
		nav = GetComponent<NavMeshAgent>();
		anim = GetComponentInChildren<Animator> ();

	}

	void Start(){
		isWalk = false;
		isDead = false;
		attack = false;
		if (gameObject.tag == "Boss") {
			AnyCollider [0].enabled = false;
			AnyGun [0].SetActive (false);
		}
		InvokeRepeating ("DamegeBobaAttack", 2.0f, 2.0f);
	}

	void Update(){
		NavManager ();
		Anim ();
		//StartCoroutine (DamegeAttack ());
	} 

	void NavManager(){
		// If the entering collider is the player...
		if(sensor.PlayerDetected){
			nav.enabled = true;
			nav.SetDestination (player.position);
			isWalk = true;
		}
		// Otherwise...
		else{
			// ... disable the nav mesh agent.
			nav.enabled = false;
			isWalk = false;
		}

		if (sphereCol.FavorableAttack) {
			if (gameObject.tag == "Boba" || gameObject.tag == "Boss") {
				nav.enabled = false;
				Vector3 targetPos = GameObject.FindWithTag ("Player").transform.position;
				targetPos.y = transform.position.y; //set targetPos y equal to mine, so I only look at my own plane
				Quaternion targetDir = Quaternion.LookRotation (targetPos - transform.position);
				transform.rotation = Quaternion.Slerp (transform.rotation, targetDir, 60 * Time.deltaTime);
				attack = true;
				StartCoroutine (DamegeAttack ());
			} else {
				nav.enabled = true;
			}
		} else {
			attack = false;
			if (gameObject.tag == "Boss") {
				AnyCollider [0].enabled = false;
			}
		}

		if(enemieHealth.HealthEnemie <= 0){
			nav.enabled = false;
			isWalk = false;
			isDead = true;
			Destroy (gameObject, 1.5f);
			AnyGun [0].SetActive (true);
		}
	}

	IEnumerator DamegeAttack(){
		if(attack && gameObject.tag == "Boss"){
			yield return new WaitForSeconds (1.0f);
			AnyCollider [0].enabled = true;
		}

	}

	void DamegeBobaAttack(){
		if(attack && attack && gameObject.tag == "Boba"){
			GameObject bulletInstance = Instantiate (AnyGun[0], AnyGun[1].transform.position, AnyGun[1].transform.rotation) as GameObject;
			bulletInstance.GetComponent<Rigidbody> ().velocity = bulletInstance.transform.forward * ProjectilSpeed;
			Destroy (bulletInstance, 3.0f);

		}
	}

	void Anim(){
		anim.SetBool ("IsWalk", isWalk);
		anim.SetBool ("IsDead", isDead);
		anim.SetBool ("IsAttack", attack);   
	}



	void OnDestroy(){
		GameObject spw = Instantiate (spwRewards[Random.Range(0, spwRewards.Length)], transform.position, transform.rotation) as GameObject;

	}  
	void OnApplicationQuit(){
		
	}
}
