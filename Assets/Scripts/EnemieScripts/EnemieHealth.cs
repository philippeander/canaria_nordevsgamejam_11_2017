﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemieHealth : MonoBehaviour {

	[SerializeField]private int healthEnemie = 100;
	[SerializeField]private int ShellDamege = 10;
	[SerializeField]private int SwordDamege = 10;
	[SerializeField]private Canvas m_canvas;

	EnemieAi enemieAi;

	public int HealthEnemie{get{return healthEnemie;}}

	void Awake(){
		enemieAi = GetComponent<EnemieAi> ();
	}

	void Start () {
		healthEnemie = 100;
	}
	// Update is called once per frame
	void Update () {
		m_canvas.GetComponentInChildren<Slider>().value = healthEnemie;
		m_canvas.transform.LookAt (Camera.main.transform.position);

	}
	void OnCollisionEnter(Collision shell){
		if(shell.gameObject.tag == "shell"){
			healthEnemie -= ShellDamege;
		}
	}

	void OnTriggerEnter( Collider other )
	{
		if(other.gameObject.tag == "Sword"){
			healthEnemie -= SwordDamege;
		}
	}
}
