﻿using System;
using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public Transform Target;
	public Transform camTransform;
	public Camera cam;
	public float distance = 4.0f;

	public float minAngleY = 0.0f;
	public float maxAngleY = 50.0f;

	private float currentX = 0.0f;
	private float currentY = 0.0f;
	private float sensivityX = 4.0f;
	private float sensivityY = 1.0f;


	// Use this for initialization
	void Start () {
		
		camTransform = transform;
		cam = Camera.main;

	}
	
	// Update is called once per frame
	void LateUpdate () {
		Vector3 dir = new Vector3 (0,0,-distance);
		Quaternion rotation = Quaternion.Euler (-currentY,currentX,0);
		camTransform.position = Target.position + rotation * dir;
		camTransform.LookAt (Target.position);
	}

	void Update(){
		currentX += Input.GetAxis ("Mouse X");
		currentY += Input.GetAxis ("Mouse Y");

		currentY = Mathf.Clamp (currentY, minAngleY, maxAngleY );
	}
}
