﻿using UnityEngine;
using System.Collections;

public class BorderControl : MonoBehaviour {

	GameObject player;
	public Vector3 Initialpos;
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		Initialpos = player.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter( Collider other )
	{
		if(other.gameObject.tag == "Player"){
			player.transform.position = Initialpos;
		}

	}


}
