﻿using UnityEngine;
using System.Collections;

public class EnemieCollider : MonoBehaviour {

	[SerializeField]private int healthEnemie = 100;
	[SerializeField]private int ShellDamege = 10;
	[SerializeField]private int SwordDamege = 10;


	public int CoHealthEnemie{get{return healthEnemie;}set{healthEnemie = value;}}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision shell){
		if(gameObject && shell.gameObject.tag == "shell"){
			healthEnemie -= ShellDamege;
		}
	}

	void OnTriggerEnter( Collider other )
	{
		if(gameObject && other.gameObject.tag == "Sword"){
			healthEnemie -= SwordDamege;
		}
	}
}
