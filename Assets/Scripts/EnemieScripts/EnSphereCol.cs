﻿using UnityEngine;
using System.Collections;

public class EnSphereCol : MonoBehaviour {

	GameObject player;
	bool favorableAttack = false;

	public bool FavorableAttack{get{ return favorableAttack; }}

	void Awake(){
		player = GameObject.FindGameObjectWithTag ("Player");
	}

	void OnTriggerEnter (Collider other){
		if (other.gameObject == player)
			favorableAttack = true;
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject == player)
			favorableAttack = false;
	}
}
