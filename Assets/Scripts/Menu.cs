﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

	void Start(){
		Cursor.visible = true;
	}

	public void BtnPlay(){
		SceneManager.LoadScene (1);
	}

	public void BtnExit(){
		Application.Quit ();
	}
}
