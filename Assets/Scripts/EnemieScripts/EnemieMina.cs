﻿using UnityEngine;
using System.Collections;

public class EnemieMina : MonoBehaviour {


	enum EnemieType{
		Cat,
		boba,
		Phanton,
		boss
	}

	[SerializeField]private EnemieType enemieType;

	[SerializeField]private GameObject cat;
	[SerializeField]private GameObject boba;
	[SerializeField]private GameObject phanton;
	[SerializeField]private GameObject boss;

	public GameObject player;
	// Use this for initialization
	void Awake () {
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter (Collider other){
		GameObject mina;
		if (other.gameObject == player) {
			switch(enemieType){
			case EnemieType.Cat:
				mina = Instantiate (cat, transform.position, transform.rotation) as GameObject;
				break;
			case EnemieType.boba:
				mina = Instantiate (boba, transform.position, transform.rotation) as GameObject;
				break;
			case EnemieType.Phanton:
				mina = Instantiate (phanton, transform.position, transform.rotation) as GameObject;
				break;
			case EnemieType.boss:
				mina = Instantiate (boss, transform.position, transform.rotation) as GameObject;
				break;
			}
			Destroy (gameObject);
		}

	}
}
