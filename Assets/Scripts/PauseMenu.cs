﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

	public GameObject canvasPause;
	public bool Pause;

	// Use this for initialization
	void Start () {
		canvasPause.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
		if(Input.GetKeyUp(KeyCode.Escape)){
			Pause = !Pause;
		}

		if (Pause) {
			canvasPause.SetActive (true);
			Time.timeScale = 0;
		} else {
			canvasPause.SetActive (false);
			Time.timeScale = 1;
		}

		if(canvasPause.activeSelf == true){
			if(Input.GetKeyUp(KeyCode.Return)){
				AbGame ();
			}
		}


	}


	public void AbGame(){
		Pause = false;
		SceneManager.LoadScene (0);
	}
}
