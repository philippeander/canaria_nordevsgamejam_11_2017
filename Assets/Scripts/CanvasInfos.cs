﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CanvasInfos : MonoBehaviour {

	public enum WhoSpeak{Urso,Girl}
	public enum MomentDialog{Part_1, Parte_2, PartFinal}
	public enum ImageOfRef{Nothing, Chave1, Chave2, Hearth, Power, Star}

	[Serializable]
	public class DialogInfos{
		//[Multiline(3)]
		[Space(10)]
		[TextArea]
		public string dialogPart;			//Variavel para mostrar o dialogo
		public WhoSpeak whoSpeak;			//Variavel para mostrar a imagem de quem fala
		public MomentDialog momentDialog;	//Variavel para dizer qual é o momento da Fala
		public ImageOfRef imageOfRef;		//Perguntar se no momento daquela fala, ela é acompanhada de alguma imagem
	}
	[Serializable]
	public class GetVisualGo
	{
		[Header("Text Game Object ")]
		[Space(10)]
		public Text textComponent;
		[Space(10)]
		[Header("IMAGES TO ILUSTRE ")]
		[Space(10)]
		public GameObject girl;
		public GameObject urso;
		[Space(10)]
		public Image boxToImage;
		public Sprite imgNull;
		public Sprite chave1;
		public Sprite chave2;
		public Sprite hearth;
		public Sprite power;
		public Sprite Star;
		[Space(10)]
		public GameObject ContinueIcon;
		public GameObject StopIcon;

	}

	[Serializable]
	public class OtherVariables
	{
		public GameObject m_canvasGroup;
		public float secondsBetweenCharacters = 0.15f;
		public float CharRateMutiplayer = 0.5f;
		public KeyCode DialogueInput_1 = KeyCode.Return;
		public KeyCode DialogueInput_2 = KeyCode.DownArrow;

	}

	[Header("TEXT INFOS")]
	[Space(10)]
	[SerializeField]private DialogInfos[] dialogInfos; 	//Array(Banco de Dados) para escrever todas as falas separadas por indices
	[Space(10)]
	[SerializeField]private GetVisualGo getVisualGameObjects;
	[Space(10)]
	[SerializeField]private OtherVariables otherVariables;



	bool isStringBeingRevealed = false;
	bool isDialoguePlaying = false;
	bool isEndOfDialogue = false;				//É ativo quando mostra a ultima fala do dialogo
	GameObject player;
	// Use this for initialization
	void Awake(){
		player = GameObject.FindGameObjectWithTag ("Player");
	}

	void Start () {
		getVisualGameObjects.textComponent.text = "";
		HideIcons ();
		otherVariables.m_canvasGroup.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Space)){
			otherVariables.m_canvasGroup.SetActive (false);
			PlayerHealth.RegressiveContdown = true;
		}
	}
		
	void OnTriggerEnter( Collider other ){
		if (other.gameObject.tag == "Player") {
			if (!isDialoguePlaying) {
				otherVariables.m_canvasGroup.SetActive (true);
				PlayerHealth.RegressiveContdown = false;
				isDialoguePlaying = true;
				StartCoroutine (StartDialogue ());
				GetComponent<SphereCollider> ().enabled = false;
			} 
		}

	} 

	//2º- Corrotina para passar o indice do array
	IEnumerator StartDialogue(){
		int dialogueLength = dialogInfos.Length;
		int currentDialogueIndex = 0;

		while(currentDialogueIndex < dialogueLength || !isStringBeingRevealed){
			if(!isStringBeingRevealed){
				isStringBeingRevealed = true;
				SwuitchImages (currentDialogueIndex);
				StartCoroutine (DisplayString(dialogInfos[currentDialogueIndex++].dialogPart));

				if(currentDialogueIndex >= dialogueLength){
					isEndOfDialogue = true;
				}
			}
			yield return 0;
		}

		while(true){
			if (Input.GetKeyDown (otherVariables.DialogueInput_1) || Input.GetKeyDown (otherVariables.DialogueInput_2))
				break;
			yield return 0;
		}

		HideIcons ();
		isEndOfDialogue = false;
		isDialoguePlaying = false;
		otherVariables.m_canvasGroup.SetActive (false);
		PlayerHealth.RegressiveContdown = true;
	}

	//3º- Corrotina para passar o indice da string
	//"Index Element" recebe a String do Array
	IEnumerator DisplayString(string IndexElement){
		int StringLength = IndexElement.Length; 	//Pega a quantidade de Char da(String) palavra ou frase
		int CurrentCharIndex = 0;				//Contador para mostrar o indice(ou letra atual) atual da string
		HideIcons();
		getVisualGameObjects.textComponent.text = ""; //Zera o TextBox cada vez que muda o indice do Array
		//Loop encarregado de adicionar a letras uma a uma até q ultimo elemento da String
		while(CurrentCharIndex < StringLength){
			getVisualGameObjects.textComponent.text += IndexElement[CurrentCharIndex]; //Vai adicionando as letra a cada loop
			CurrentCharIndex++;
			//Aqui ele força o loop dar um tempo de espera para dar o efeito de digitação
			if (CurrentCharIndex < StringLength) { 
				//Esse If serve para acelerar a passagem do dialogo, caso contrario, ele continuará com a velocidade pre-estabelecida
				if (Input.GetKey(otherVariables.DialogueInput_1) || Input.GetKeyDown (otherVariables.DialogueInput_2)) {
					yield return new WaitForSeconds (otherVariables.secondsBetweenCharacters * otherVariables.CharRateMutiplayer); 
				} else {
					yield return new WaitForSeconds (otherVariables.secondsBetweenCharacters); 
				}
			} else {
				break;
			}
		}
		ShowIcon (); //quando terminar de mostrar a frase ele mostra os icones de  finalizar a fala
		while(true){
			//enquanto ele não apertar o botão o dialogo não corre e pemanece na tela
			if (Input.GetKeyDown (otherVariables.DialogueInput_1) || Input.GetKeyDown (otherVariables.DialogueInput_2))
				break;
			yield return 0;
		}
		HideIcons (); //quando apertar ele esconde os icone para passar para o proximo
		isStringBeingRevealed = false;
		getVisualGameObjects.textComponent.text = "";

	}

	void HideIcons(){
		getVisualGameObjects.ContinueIcon.SetActive (false);
		getVisualGameObjects.StopIcon.SetActive (false);
	}
	void ShowIcon(){
		if(isEndOfDialogue){
			getVisualGameObjects.StopIcon.SetActive(true);
			return;
		}
		getVisualGameObjects.ContinueIcon.SetActive (true);
	}

	void SwuitchImages(int indexElement){

		switch(dialogInfos[indexElement].whoSpeak){
		case WhoSpeak.Urso:
			getVisualGameObjects.urso.SetActive (true);
			getVisualGameObjects.girl.SetActive (false);
			break;
		case WhoSpeak.Girl:
			getVisualGameObjects.girl.SetActive (true);
			getVisualGameObjects.urso.SetActive (false);
			break;
		}

		switch(dialogInfos[indexElement].imageOfRef){
		case ImageOfRef.Nothing:
			getVisualGameObjects.boxToImage.sprite = getVisualGameObjects.imgNull;
			break;
		case ImageOfRef.Chave1:
			getVisualGameObjects.boxToImage.sprite = getVisualGameObjects.chave1;
			break;
		case ImageOfRef.Chave2:
			getVisualGameObjects.boxToImage.sprite = getVisualGameObjects.chave2;
			break;
		case ImageOfRef.Hearth:
			getVisualGameObjects.boxToImage.sprite = getVisualGameObjects.hearth;
			break;
		case ImageOfRef.Power:
			getVisualGameObjects.boxToImage.sprite = getVisualGameObjects.power;
			break;
		case ImageOfRef.Star:
			getVisualGameObjects.boxToImage.sprite = getVisualGameObjects.Star;
			break;
		}
	}

}
