﻿using UnityEngine;
using System.Collections;

public class EnSensor : MonoBehaviour {

	GameObject player;
	bool playerDetected = false;

	public bool PlayerDetected{get{ return playerDetected; }}

	void Awake(){
		player = GameObject.FindGameObjectWithTag ("Player");
	}

	void OnTriggerEnter (Collider other){
		if (other.gameObject == player)
			playerDetected = true;
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject == player)
			playerDetected = false;
	}
}
