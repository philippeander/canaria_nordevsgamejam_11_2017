﻿using UnityEngine;
using System.Collections;

public class SinePlatform : MonoBehaviour {
	public float speed = 1.5f;
	public Transform Point_1;
	public Transform Point_2;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = Vector3.Lerp (Point_1.position, Point_2.position, Mathf.PingPong(Time.time*speed, 1.0f));
	}

	void OnTriggerEnter(Collider other){
		if(other.tag == "Player" ){
			other.transform.parent = transform;
		}
	}

	void OnTriggerExit(Collider other){
		if(other.tag == "Player"){
			other.transform.parent = null;
		}
	}
}
