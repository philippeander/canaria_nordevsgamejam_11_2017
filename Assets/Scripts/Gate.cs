﻿using UnityEngine;
using System.Collections;

public class Gate : MonoBehaviour {

	[SerializeField]private GameObject GateLight_1;
	[SerializeField]private GameObject GateLight_2;
	[SerializeField]private Material MatChave_1;
	[SerializeField]private Material MatChave_2;
	[SerializeField]private bool getChave_1 = false;
	[SerializeField]private bool getChave_2 = false;
	Animator anim;

	public bool GetChave_1{get{return getChave_1;}set{getChave_1 = value;}}
	public bool GetChave_2{get{return getChave_2;}set{getChave_2 = value;}}


	void Awake(){
		anim = GetComponentInChildren<Animator> ();
	}
	// Use this for initialization
	void Start () {
		
		getChave_1 = false;
		getChave_2 = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(getChave_1){
			GateLight_1.GetComponent<MeshRenderer> ().material= MatChave_1;
		}
		if(getChave_2){
			GateLight_2.GetComponent<MeshRenderer> ().material = MatChave_2;
		}
		if(getChave_1 && getChave_2){
			StartCoroutine (OpenDoor());
		}
	}

	IEnumerator OpenDoor(){
		yield return new WaitForSeconds (2f);
		anim.SetBool ("Open", true);
	}
}
