﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class CharacterController : MonoBehaviour {

	[Serializable]
	public class MoveSettins
	{
		public float forwardVel = 12f;
		public float rotateVel = 100f;
		public float JumpVel = 25;
		public float distToGrounded = 0.1f;
		public LayerMask ground;

	}

	[Serializable]
	public class PhysicsSettins
	{
		public float downAccel = 0.75f;
	}

	[Serializable]
	public class InputSettins
	{
		public float inputDelay = 0.1f;
		public string FORWARD_AXIS = "Vertical";
		public string TURN_AXIS = "Horizontal";
		public string JUMP_AXIS = "Jump";
	}

	public MoveSettins moveSettins = new MoveSettins ();
	public PhysicsSettins physicsSettins = new PhysicsSettins ();
	public InputSettins inputSettins = new InputSettins ();

	[Header("Attack")]
	public GameObject bullet;
	public GameObject instBollet;
	public float speedBullet = 20.0f;
	public GameObject mira;
	public GameObject golpeEspada;
	public GameObject gameOver;
	RaycastHit hitInfo;
	Vector3 endPointRaycast;

	PlayerHealth playerHealth;
	Vector3 velocity = Vector3.zero;
	Quaternion targetRotation;
	Rigidbody rBody;
	float forwardInput, turnInput, jumpInput;
	Animator anim;
	AnimatorClipInfo clip; 
	bool run, isGrounded, attack_1, attack_2, IsDead;

	public Quaternion TargetRotation{
		get{ return targetRotation; }
	}

	bool Grounded(){
		return Physics.Raycast (transform.position, Vector3.down, moveSettins.distToGrounded, moveSettins.ground);
	}

	void Awake(){
		anim = GetComponentInChildren<Animator> ();
		playerHealth = GetComponent<PlayerHealth> ();
	}

	void Start(){
		Cursor.visible = false;
		golpeEspada.SetActive (false);

		targetRotation = transform.rotation;
		if (GetComponent<Rigidbody> ())
			rBody = GetComponent<Rigidbody> ();
		else
			Debug.LogError ("The characte needs a rigidbody");

		forwardInput = turnInput = jumpInput = 0;

		StartCoroutine (Wakeup());
	}
	void Update(){
		GetInput ();
		Turn ();
		Anim ();
		Dead ();

		Ray ray = new Ray (mira.transform.position, mira.transform.forward);
		Debug.DrawRay (mira.transform.position, mira.transform.forward * 20f, Color.cyan );
		endPointRaycast  = ray.origin + (ray.direction * 20f);
		if(Physics.Raycast(ray, out hitInfo, 20f)){
			if (hitInfo.collider.tag == "Enemie") {
				Debug.Log ("ENEMIE DETECTED!!!! <<<<<");
			}
			if (Input.GetKeyUp (KeyCode.Mouse2))
				Debug.Log ("END OF RAY" + endPointRaycast);
		}
	}

	void FixedUpdate(){
		Run ();
		Jump ();

		rBody.velocity = transform.TransformDirection (velocity);
		//rBody.velocity = velocity;

		Debug.DrawRay (transform.position, Vector3.down * moveSettins.distToGrounded, Color.red);
		
		if(!Grounded()){
			print ("Is GrOUNDED");
		}

	}

	void GetInput(){
		forwardInput = Input.GetAxis (inputSettins.FORWARD_AXIS); //interpolated
		turnInput = Input.GetAxis (inputSettins.TURN_AXIS); //interpolated
		jumpInput = Input.GetAxis (inputSettins.JUMP_AXIS); //non-interpolated

		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			attack_2 = true;
			StartCoroutine ("GolpeEspada");
		}
		if (Input.GetKeyUp (KeyCode.Mouse0)) {
			attack_2 = false;
		}

		if (Input.GetKeyDown (KeyCode.Mouse1)) {
			attack_1 = true;
			Shoot ();
		}
		if (Input.GetKeyUp (KeyCode.Mouse1)) {
			attack_1 = false; 
		}
			
	}

	void Run(){
		if (Mathf.Abs (forwardInput) > inputSettins.inputDelay) {
			velocity.z = moveSettins.forwardVel * forwardInput;
			run = true;
		}
		else if (Mathf.Abs (turnInput) > inputSettins.inputDelay) {
			velocity.x = moveSettins.forwardVel * turnInput;
			run = true;
		}
		else {
			velocity.x = 0;
			velocity.z = 0;
			run = false;

		}




	}

	void Turn(){
//		if (Mathf.Abs (turnInput) > inputSettins.inputDelay) {
//			targetRotation *= Quaternion.AngleAxis (moveSettins.rotateVel * turnInput * Time.deltaTime, Vector3.up);
//		}
//		transform.rotation = targetRotation;



		Vector3 targetPos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));;
		targetPos.y = transform.position.y; //set targetPos y equal to mine, so I only look at my own plane
		Quaternion targetDir = Quaternion.LookRotation( transform.position - targetPos);
		transform.rotation = Quaternion.Slerp(transform.rotation, targetDir, moveSettins.rotateVel * Time.deltaTime);
	}

	void Jump(){
		if (jumpInput > 0 && Grounded ()) {
			//Jump
			velocity.y = moveSettins.JumpVel;

		} else if (jumpInput == 0 && Grounded ()) {
			//zero out our velocity.y
			velocity.y = 0;
		} else {
			//decrase velocity.y
			velocity.y -= physicsSettins.downAccel;

		}
	}

	void Anim(){
		
		anim.SetBool ("Run", run);
		anim.SetBool ("IsGrounded", isGrounded);
		anim.SetBool ("Attack_1", attack_1);
		anim.SetBool ("Attack_2", attack_2);
		anim.SetBool ("IsDead", IsDead);

		if (Grounded ())
			isGrounded = true;
		else
			isGrounded = false;
	}
	IEnumerator Wakeup(){
		anim.SetBool ("Wakeup", true);
		yield return new WaitForSeconds (3.0f);
		anim.SetBool ("Wakeup", false);
	}

	void Shoot(){
		if (playerHealth.Power > 0) {
			GameObject bulletInstance = Instantiate (bullet, instBollet.transform.position, instBollet.transform.rotation) as GameObject;
			bulletInstance.transform.LookAt (endPointRaycast);
			bulletInstance.GetComponent<Rigidbody> ().velocity = bulletInstance.transform.forward * speedBullet;
			Destroy (bulletInstance, 3.0f);
			playerHealth.Power -= playerHealth.PowerSub;
		}
	}
	private IEnumerator GolpeEspada(){
		golpeEspada.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		golpeEspada.SetActive (false);
	}

	void Dead(){
		if(playerHealth.Life <= 0 || playerHealth.Spirit <= 0){
			StartCoroutine (GameOver());
		}
	}
	IEnumerator GameOver(){
		IsDead = true;
		yield return new WaitForSeconds (4.0f);
		gameOver.SetActive (true);
		StartCoroutine (GoToMenu());
	}

	IEnumerator GoToMenu(){
		yield return new WaitForSeconds (3.0f);
		SceneManager.LoadScene (0);
	}
}
