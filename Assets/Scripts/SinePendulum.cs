﻿using UnityEngine;
using System.Collections;

public class SinePendulum : MonoBehaviour {


	public float angle = 40.0f;
	public float speed = 1.5f;

	Quaternion qStart, qEnd;

	void Start () {
		qStart = Quaternion.AngleAxis ( angle, Vector3.left);
		qEnd   = Quaternion.AngleAxis (-angle, Vector3.left);
	}

	void Update () {
		transform.localRotation = Quaternion.Lerp (qStart, qEnd, (Mathf.Sin(Time.time * speed) + 1.0f) / 2.0f);
	}
}
