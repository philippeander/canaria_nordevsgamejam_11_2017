﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour {
	[Header("Slides")]
	[SerializeField]private Slider sldLife; 
	[SerializeField]private Slider sldPower;
	[SerializeField]private Slider sldSpirit;
	[Header("Keys")]
	[SerializeField]private Image imgChave_1; 
	[SerializeField]private Image imgChave_2;
	[Header("Damage By Enamie")]
	[SerializeField]private int damageCat = 5;
	[SerializeField]private int damageBoba = 5;
	[SerializeField]private int damageBobaBullet = 2;
	[SerializeField]private int damagePhanton = 5;
	[SerializeField]private int damageBoss = 5;
	[Header("Value By Reward")]
	[SerializeField]private int ReLife = 3;
	[SerializeField]private int RePower = 3;
	[SerializeField]private int ReSpirit = 3;
	[Header("Keys Color")]
	[SerializeField]private Color inativo = new Color(255, 255, 255, 33);
	[SerializeField]private Color ativo = new Color(255, 255, 255, 33);
	[Header("Health Limit")]
	[SerializeField]private int life = 100;
	[SerializeField]private int power = 100;
	[SerializeField]private int spirit = 100;
	[Header("Power Effect")]
	[SerializeField]private int powerSub = 2;
	[Header("Others")]
	[SerializeField]private GameObject cameraGate;
	[SerializeField]private GameObject gate;
	[SerializeField]private GameObject endOfGame;
	[SerializeField]private Vector3 CheckPoint;

	public int Power{get{return power;}set{power = value;}}
	public int PowerSub{get{return powerSub;}set{powerSub = value;}}
	public int Life{get{return life;}set{life = value;}}
	public int Spirit{get{return spirit;}set{spirit = value;}}

	public bool chave_1;
	public bool chave_2;


	public static bool RegressiveContdown; //STATIC

	void Awake(){
		cameraGate = GameObject.FindGameObjectWithTag ("CameraGate");
		gate = GameObject.FindGameObjectWithTag ("Gate");
	}

	void Start () {
		life = 90;
		power = 90;
		spirit = 90;
		imgChave_1.color = inativo;
		imgChave_2.color = inativo;
		chave_1 = false;
		chave_2 = false;
		RegressiveContdown = true;
		cameraGate.SetActive (false);
		InvokeRepeating ("SubSprit", 2, 2);
		CheckPoint = gameObject.transform.position;
	}
	void Update () {
		sldLife.value = life;
		sldPower.value = power;
		sldSpirit.value = spirit;
		HealthManager ();
		gate.GetComponent<Gate> ().GetChave_1 = chave_1;
		gate.GetComponent<Gate> ().GetChave_2 = chave_2;

	}
	void OnTriggerEnter( Collider other ){
		if(other.gameObject.tag == "Cat"){life -= damageCat;}
		if(other.gameObject.tag == "Phanton"){life -= damagePhanton;}
		if(other.gameObject.tag == "Boba"){life -= damageBoba;}
		if(other.gameObject.tag == "Axe"){life -= damageBoss;}


		if(other.gameObject.tag == "Chave_1"){
			chave_1 = true;
			Destroy (other.gameObject);
			StartCoroutine (LookGate());
		}
		if(other.gameObject.tag == "Chave_2"){
			chave_2 = true;
			Destroy (other.gameObject);
			StartCoroutine (LookGate ());
		}

		if(other.gameObject.tag == "Life"){
			life += ReLife;
			Destroy (other.gameObject);}
		if(other.gameObject.tag == "Power"){
			power += RePower;
			Destroy (other.gameObject);}
		if(other.gameObject.tag == "Spirit"){
			spirit += ReSpirit;
			Destroy (other.gameObject);}
		if(other.gameObject.tag == "BorderControl"){
			gameObject.transform.position = CheckPoint;}
		if(other.gameObject.tag == "CheckPoint"){
			CheckPoint = other.gameObject.transform.position;}
	} 

	void OnCollisionEnter(Collision other){
		if (other.gameObject.tag == "shellBoba") {
			life -= damageBobaBullet;
		}
		if(other.gameObject.tag == "Portal"){
			StartCoroutine (EndOfGame());
		}
	}

	void HealthManager(){
		if(life <= 0){ }
		if(life >= 100){life = 100;}

		if(power <= 0){power = 0;}
		if(power >= 100){power = 100;}

		if(spirit <= 0){spirit = 0;}
		if(spirit >= 100){spirit = 100;}

		if(chave_1 == true){imgChave_1.color = ativo;}
		if(chave_2 == true){imgChave_2.color = ativo;}
		if(chave_1 == false){imgChave_1.color = inativo;}
		if(chave_2 == false){imgChave_2.color = inativo;}

		//Metodo para geerenciar a contagem reressiva das variaveis Power e Spirit
		//Lembrar de observar o Start Como está

	}

	void SubSprit(){
		//Metodo para geerenciar a contagem reressiva das variaveis Power e Spirit
		//Lembrar de observar o Start Como está
		if(RegressiveContdown){
			spirit -= 1;
		}
	}

	IEnumerator LookGate(){
		cameraGate.SetActive (true);
		yield return new WaitForSeconds (3.0f);
		cameraGate.SetActive (false);
	}

	IEnumerator EndOfGame(){
		endOfGame.SetActive (true);
		yield return new WaitForSeconds (2.0f);
		SceneManager.LoadScene (0);
	}


}
